﻿SpeechRecognition Readme
=============================

##MAC SUPPORT MAY BE BROKEN, WE NEED TESTERS

###Introduction

This is a simple Perl module that interfaces with the Google Speech Recognition API. It sends a FLAC file with the speech, either directly from the mic or from a FLAC file provided, using the methods `interpret_from_mic` or `interpret_from_file`, respectively. 



#####Planned Changes:

 - Check if file is FLAC; if not, convert it to FLAC

=======
 - OS X support
 - Dependency checker

###Installation

To install this module type the following:

`perl Makefile.PL`  
`make`  
`make test`  
`make install`  

###Dependencies

This module requires these other modules and libraries:

 - JSON::Parse
 - flac
 - arecord (ALSA) (Linux only)
 - sox (OS X only)
 - arecord (ALSA)


###Copyright & License

Copyright © 2013 Liam Crabbe


Support for OS X added by Shawn Anastasio @shawnanastasio

=======

Credit to @theracermaster for reformatting my readme (:

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.4 or,
at your option, any later version of Perl 5 you may have available.
