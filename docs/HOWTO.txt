speech-recognition-perl is a perl module that allows you to interface with Google's Speech API.

Currently, there are two methods, interpret_from_mic and interpret_from_file which get audio from mic or a flac file respectively and upload it to Google's Speech API.
You can call interpret_from_mic with a value in seconds to determine how long to record. Ex interpret_from_mic(3) to record for 3 seconds.

If no value is specified, sound will be recorded until there are 3 consecutive seconds of silence (OS X Only at the moment).


